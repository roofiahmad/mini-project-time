import React, { Component } from 'react';
import './PaginationComp.css';
import { Link } from 'react-router-dom';
import { Container } from 'reactstrap';

class PaginationComp extends Component {
  render() {
    return (
      <Container className="mt-5">
      <div className="container">
      <nav aria-label="Page navigation">
        <ul className="pagination container justify-content-center">
          <li classNameName="page-item">
            <Link style={{backgroundColor:"#FFB700"}} className="page-link text-white" href="#" aria-label="Previous">
              <span aria-hidden="true">&larr;</span>
              <span className="sr-only">Previous</span>
            </Link>
          </li>
          <li className="page-item"><Link style={{backgroundColor:"#FFB700"}} className="page-link text-white" href="#">1</Link></li>
          <li className="page-item"><Link style={{backgroundColor:"#FFB700"}} className="page-link text-white" href="#">2</Link></li>
          <li className="page-item"><Link style={{backgroundColor:"#FFB700"}} className="page-link text-white" href="#">3</Link></li>
          <li className="page-item"><Link style={{backgroundColor:"#FFB700"}} className="page-link text-white" href="#">4</Link></li>
          <li className="page-item"><Link style={{backgroundColor:"#FFB700"}} className="page-link text-white" href="#">5</Link></li>
          <li className="page-item"><Link style={{backgroundColor:"#FFB700"}} className="page-link text-white" href="#">6</Link></li>
          <li className="page-item">
            <Link style={{backgroundColor:"#FFB700"}} className="page-link text-white" href="#" aria-label="Next">
              <span aria-hidden="true">&rarr;</span>
              <span className="sr-only">Next</span>
            </Link>
          </li>
        </ul>
      </nav>
      </div>
      </Container>
    );
  }
}

export default PaginationComp;