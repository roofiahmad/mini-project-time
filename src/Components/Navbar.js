import React, { Component } from 'react';
// import './Navbar.css';
import Overview from '../Pages/Overview';
import Review from '../Pages/Review';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import classCss from "../Components/FilmCategory.module.css";



class Navbar extends Component {
  render() {
    return (
      <Router>
        <nav className="container mt-5">
          <div>
            <div>
            <Link to="/overview" className="nav-item active">
              <button className={classCss.category_btn}>Overview</button>
            </Link>
            <Link to="/review">
              <button className={classCss.category_btn}>Review</button>
            </Link>
            </div>
          </div>
        </nav>

        <Switch>
        <Route path="/overview">
          <Overview />
        </Route>
        <Route path="/review">
          <Review />
        </Route>
        </Switch>
      </Router>
    );
  }
}

export default Navbar;