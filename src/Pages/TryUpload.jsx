import React, { Component } from "react";

export default class TryUpload extends Component {
  state = {
    image: null,
  };

  handleFile = (e) => {
    let file = e.target.files[0];
    this.setState({
      image: file,
    });
  };
  handleUpload = (e) => {
    let file = this.state.image;
    let formData = new FormData();
    formData.append("image", file);
    formData.append("name", "Roofi");
    console.log(formData);
    e.preventDefault();
  };

  render() {
    console.log(this.state.image);
    return (
      <div>
        <form
          onSubmit={(e) => {
            this.handleUpload(e);
          }}
        >
          <label> Select File</label>
          <input type="file" name="file" onChange={(e) => this.handleFile(e)} />
          <button type="submit">Submit</button>
        </form>
      </div>
    );
  }
}
